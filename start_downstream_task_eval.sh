model_dir=models/1512051714feats_transVgg16big_nsampling_same_spk_win32_neg_samples4_lcontexts2_rcontexts2_flts40_embsize100_fc_size512_unit_norm_var_dropout_keep0.9_l2_reg0.0001_dot_combine

# random sampling
# model_dir=models/1512051734feats_transVgg16big_nsampling_rnd_win32_neg_samples4_lcontexts2_rcontexts2_flts40_embsize100_fc_size512_unit_norm_var_dropout_keep0.9_l2_reg0.0001_dot_combine

# --utt2spk /scratch/savee/utt2spk

python unsup_model_neg.py --downstreamtask True --train_dir $model_dir --filelist /scratch/savee/SAVEE_feats.ark --utt2spk /scratch/savee/utt2spk --utt2set /scratch/savee/utt2set  --utt2class /scratch/savee/utt2class7 --noend_to_end --fc_size 512 --downstreamtask_repeats 10 --downstreamtask_batchsize 32 --downstreamtask_hiddenlayers 0 --downstreamtask_trainepochs 5000 --window_length 32 --window_neg_length 32  --downstreamtask_use_preouts True

# --downstreamtask_speakersplit True
# --downstreamtask_use_preouts True
