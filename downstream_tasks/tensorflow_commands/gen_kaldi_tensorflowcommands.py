import os
import re
import hashlib
import sys

MAX_NUM_WAVS_PER_CLASS = 2**27 - 1  # ~134M

def which_set(filename, validation_percentage=10.0, testing_percentage=10.0):
  """Determines which data partition the file should belong to.

  We want to keep files in the same training, validation, or testing sets even
  if new ones are added over time. This makes it less likely that testing
  samples will accidentally be reused in training when long runs are restarted
  for example. To keep this stability, a hash of the filename is taken and used
  to determine which set it should belong to. This determination only depends on
  the name and the set proportions, so it won't change as other files are added.

  It's also useful to associate particular files as related (for example words
  spoken by the same person), so anything after '_nohash_' in a filename is
  ignored for set determination. This ensures that 'bobby_nohash_0.wav' and
  'bobby_nohash_1.wav' are always in the same set, for example.

  Args:
    filename: File path of the data sample.
    validation_percentage: How much of the data set to use for validation.
    testing_percentage: How much of the data set to use for testing.

  Returns:
    String, one of 'train', 'dev', or 'test'.
  """
  base_name = os.path.basename(filename)
  # We want to ignore anything after '_nohash_' in the file name when
  # deciding which set to put a wav in, so the data set creator has a way of
  # grouping wavs that are close variations of each other.
  hash_name = re.sub(r'_nohash_.*$', '', base_name)
  # This looks a bit magical, but we need to decide whether this file should
  # go into the training, testing, or validation sets, and we want to keep
  # existing files in the same set even if more files are subsequently
  # added.
  # To do that, we need a stable way of deciding based on just the file name
  # itself, so we do a hash of that and then use that to generate a
  # probability value that we use to assign it.

  print('hash_name:',hash_name)

  hash_name_hashed = hashlib.sha1(hash_name.encode('utf-8')).hexdigest()
  percentage_hash = ((int(hash_name_hashed, 16) %
                      (MAX_NUM_WAVS_PER_CLASS + 1)) *
                     (100.0 / MAX_NUM_WAVS_PER_CLASS))
  if percentage_hash < validation_percentage:
    result = 'dev'
  elif percentage_hash < (testing_percentage + validation_percentage):
    result = 'test'
  else:
    result = 'train'
  return result

wav_scp_template = "sox $filepath -t wav -r 16k -b 16 -e signed - |"


#session. Twenty core command words were recorded, with most speakers saying each
#of them five times. The core words are "Yes", "No", "Up", "Down", "Left",
#"Right", "On", "Off", "Stop", "Go", "Zero", "One", "Two", "Three", "Four",
#"Five", "Six", "Seven", "Eight", and "Nine". To help distinguish unrecognized
#words, there are also ten auxiliary words, which most speakers only said once.
#These include "Bed", "Bird", "Cat", "Dog", "Happy", "House", "Marvin", "Sheila",
#"Tree", and "Wow".


extended_class2num = {'Yes':'0','No':'1', 'Up':'2','Down':'3', 'Left':'4', 'Right':'5', 'On':'6', 'Off':'7', 'Stop':'8',
 'Go':'9', 'Zero':'10', 'One':'11', 'Two':'12', 'Three':'13', 'Four':'14', 'Five':'15', 'Six':'16', 'Seven':'17', 'Eight':'18',
 'Nine':'19', 'Bed':'20', 'Bird':'20', 'Cat':'20','Dog':'20', 'Happy':'20','House':'20','Marvin':'20','Sheila':'20','Tree':'20','Wow':'20','_background_noise_':'21'}

#The standard chosen for the TensorFlow speechcommands example code is to look for the ten words"Yes", "No", "Up", "Down", "Left", "Right", "On","Off", "Stop", and "Go", and have one additionalspecial label for “Unknown Word”, and another for“Silence” (no speech detected)

class2num = {'Yes':'0','No':'1', 'Up':'2','Down':'3', 'Left':'4', 'Right':'5', 'On':'6', 'Off':'7', 'Stop':'8',
 'Go':'9', 'Zero':'10', 'One':'10', 'Two':'10', 'Three':'10', 'Four':'10', 'Five':'10', 'Six':'10', 'Seven':'10', 'Eight':'10',
 'Nine':'10', 'Bed':'10', 'Bird':'10', 'Cat':'10','Dog':'10', 'Happy':'10','House':'10','Marvin':'10','Sheila':'10','Tree':'10','Wow':'10','_background_noise_':'11'}


for elem in list(class2num.keys()):
    class2num[elem.lower()] = class2num[elem]


my_classes = []

line_count = 0

kaldi_ids_encountered = {}

with open("tf_commands_filelist.txt") as in_file, open('wav.scp','w') as wav_scp, open('text', 'w') as text, open('utt2spk', 'w') as utt2spk, open('spk2utt', 'w') as spk2utt, open('utt2class', 'w') as utt2class, open('utt2class_raw', 'w') as utt2class_raw , open('utt2set', 'w') as utt2set:
    for line in in_file:
        if line[-1] == '\n':
            line = line[:-1]
        print(line)
        filename = line
        line_split = line.split('/')
        myid = line_split[-1].split('.')[0]
        myclass = line_split[2]
        speaker = re.sub(r'_nohash_.*$', '', myid)

        kaldiid = speaker + '_' + myid + '_' + myclass

        if kaldiid in kaldi_ids_encountered:
            print('found duplicate id:', kaldiid)
            print('error, aborting...')
            sys.exit(-1)

        kaldi_ids_encountered[kaldiid] = True

        print(speaker, myid, line)

        wav_scp.write(kaldiid+' '+wav_scp_template.replace('$filepath',filename)+'\n')
        spk2utt.write(speaker+' '+kaldiid+'\n')
        utt2spk.write(kaldiid+' '+speaker+'\n')
        text.write(kaldiid+' dummy\n')
        utt2class_raw.write(kaldiid+' '+myclass+'\n')
        print(myclass)
        utt2class.write(kaldiid+' '+class2num[myclass]+'\n')
        utt2set.write(kaldiid+' '+which_set(line)+'\n')

        my_classes.append(myclass)

        line_count +=1

print('Done with', line_count, 'entries.')

print('Encountered the following classes:', list(set(my_classes)))
