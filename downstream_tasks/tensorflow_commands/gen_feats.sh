#ln -sn /noraid/kaldi/egs/wsj/s5/utils utils
#ln -sn /noraid/kaldi/egs/wsj/s5/steps steps

. path.sh

./utils/fix_data_dir.sh .
./steps/make_fbank.sh --fbank-config fbank.conf  .
copy-feats scp:feats.scp ark:feats.ark 
